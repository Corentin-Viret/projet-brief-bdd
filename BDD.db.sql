BEGIN TRANSACTION;


CREATE TABLE Clients (
    Id INTEGER PRIMARY KEY NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    pass VARCHAR(100) NOT NULL UNIQUE,
    role VARCHAR(100) NOT NULL ,
    date VARCHAR(80) NOT NULL,
    type INTEGER NOT NULL
);



CREATE TABLE Addresses (
    Id INTEGER PRIMARY KEY NOT NULL,
    name_pro VARCHAR(100),
    prenom VARCHAR(100) NOT NULL,
    nom VARCHAR(255) NOT NULL,
    tel REAL NOT NULL UNIQUE,
    rue VARCHAR(255) NOT NULL,
    code_postal INTEGER NOT NULL,
    ville VARCHAR(255) NOT NULL,
    pays VARCHAR(255) NOT NULL, 
    étas VARCHAR(50) NOT NULL
    
);

CREATE TABLE Association (
Id_Clients INTEGER,
Id_Addresses INTEGER,
type VARCHAR(50) NOT NULL,
PRIMARY KEY (Id_Clients, Id_Addresses)
FOREIGN KEY (Id_Clients) REFERENCES Clients(Id)
FOREIGN KEY (Id_Addresses) REFERENCES Addresses(Id)
    
);


COMMIT;





